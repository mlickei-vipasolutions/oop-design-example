package example;

/**
 * This is the main class. Only meant to be used to test compilation or to test run something.
 * Doesn't need any code inside of it.
 *
 * @author Matthew Lickei (mlickei@vipasolutions.com)
 */
public class OopDesignExample
{

    public static void main(String args[])
    {
        System.out.println("It runs!");
    }

}
