package player;

/**
 * You can delete this class if you want. It's only here to provide an example of what you'll need in your classes.
 * You'll need:
 *      - All properties
 *      - A helpful constructor
 *      - Setters and getters
 *
 * @author Matthew Lickei (mlickei@vipasolutions.com)
 */
public class ExamplePlayer
{

    //TODO include all properties
    private String _name;
    private String _age;

    //TODO include helpful constructor
    public ExamplePlayer(String name)
    {
        _name = name;
    }

    //TODO include setters/getters
    public String getAge()
    {
        return _age;
    }

    public void setAge(String age)
    {
        _age = age;
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        _name = name;
    }

}
